FROM openjdk:8-jre
VOLUME /tmp
WORKDIR /data
COPY target/agri-reference-data-api.jar  /data
EXPOSE 8000
ENTRYPOINT ["java" ,"-jar", "agri-reference-data-api.jar"]