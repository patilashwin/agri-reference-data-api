package com.agri.reference.data.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReferenceDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReferenceDataApplication.class, args);
	}

}
