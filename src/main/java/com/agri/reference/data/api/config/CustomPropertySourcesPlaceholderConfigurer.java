package com.agri.reference.data.api.config;

import java.io.IOException;
import java.util.Properties;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class CustomPropertySourcesPlaceholderConfigurer extends PropertySourcesPlaceholderConfigurer {

	private ConfigurableEnvironment environment;

	@Override
	public void setEnvironment(Environment environment) {
		super.setEnvironment(environment);
		this.environment = (ConfigurableEnvironment) environment;
	}

	@Override
	protected void loadProperties(Properties props) throws IOException {
		this.localOverride = true;
		log.info("************ Start ******************");
		for (PropertySource<?> propertySource : environment.getPropertySources()) {
			if (propertySource instanceof EnumerablePropertySource) {
				String[] propertyNames = ((EnumerablePropertySource<?>) propertySource).getPropertyNames();
				for (final String propertyName : propertyNames) {
					final String propertyValue = propertySource.getProperty(propertyName).toString();
					log.info(propertyName +  " : " + propertyValue);
				}
			}
		}
		log.info("************** End ****************");
	}

}
