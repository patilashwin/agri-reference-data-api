package com.agri.reference.data.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.agri.reference.data.api.model.Country;
import com.agri.reference.data.api.service.CountryService;

@RestController
@RequestMapping("/country")
public class CountryController {
	
	@Autowired
	CountryService service;
	
	@GetMapping("/id")
	public Country getCountryById(@PathVariable("id") int id) {
		return service.getCountryById(id);
	}
	
	@GetMapping
	public List<Country> getAllCountries(){
		return service.getAllCountries();
	}
	
	@PostMapping
	public Country save(@RequestBody Country country) {
		return service.save(country);
	}

}
