package com.agri.reference.data.api.exception;

public class CountryNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CountryNotFoundException(String countryId) {
		super(countryId);
	}

}
