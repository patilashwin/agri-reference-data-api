package com.agri.reference.data.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.agri.reference.data.api.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

}
