package com.agri.reference.data.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agri.reference.data.api.exception.CountryNotFoundException;
import com.agri.reference.data.api.model.Country;
import com.agri.reference.data.api.repo.CountryRepository;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepository;

	public Country getCountryById(int id) {
		return countryRepository.findById(id).orElseThrow(() -> new CountryNotFoundException(String.valueOf(id)));
	}

	public List<Country> getAllCountries() {
		return countryRepository.findAll();
	}

	public Country save(Country country) {
		return countryRepository.save(country);
	}

}
